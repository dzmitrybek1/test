import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DocViewerComponent } from './doc-viewer/doc-viewer.component';
import { DocInputComponent } from './doc-viewer/components/doc-input/doc-input.component';
import { DocDropdownComponent } from './doc-viewer/components/doc-dropdown/doc-dropdown.component';

@NgModule({
  declarations: [
    AppComponent,
    DocViewerComponent,
    DocInputComponent,
    DocDropdownComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
