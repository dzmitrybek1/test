import { Component } from '@angular/core';
import { BackendService } from './doc-viewer/services/backend';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'test-doc-viewer';

  constructor() {}
}
