import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { forkJoin } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { PeoAnnotation, PeoDocumentInfo } from './models/interfaces';
import { BackendService } from './services/backend';

@Component({
  selector: 'app-doc-viewer',
  templateUrl: './doc-viewer.component.html',
  styleUrls: ['./doc-viewer.component.css']
})
export class DocViewerComponent implements OnInit {
  docId = '8df97638e0363612a2849baea3b4c987';
  imageSourceUrl: SafeUrl = '';
  isDocLoading = false;

  docControls: PeoAnnotation[] = [];

  constructor(
    public backendService: BackendService,
    private sanitizer: DomSanitizer,
  ) { }

  ngOnInit(): void {
    this.isDocLoading = true;

    this.backendService.getDocumentInfo(this.docId)
      .pipe(
        tap((docInfo) => console.log('docInfo', docInfo)),
        switchMap((docInfo) => forkJoin([
          this.backendService.getPageInfo(this.docId, docInfo.pages[0].id),
          this.backendService.getPageImage(this.docId, docInfo.pages[0].id, docInfo.pages[0].width, docInfo.pages[0].height),
        ])),
        tap(() => this.isDocLoading = false),
      ).subscribe(([pageInfo, rawDocImgSource]) => {
        console.log('pageInfo', pageInfo);
        this.imageSourceUrl = this.sanitizer.bypassSecurityTrustUrl(rawDocImgSource);

        this.docControls = pageInfo.annotations;
      });
  }

  getPositionValue(position: number): string {
    return `${position}px`
  }

}

