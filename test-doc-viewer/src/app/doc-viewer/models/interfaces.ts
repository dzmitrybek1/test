interface RectObj {
  width: number;
  height: number;
}

type RectArray = [number, number, number, number];

export interface PeoDocumentInfo {
  pages: Array<PeoPageRect>;
}

export interface PeoPageRect extends RectObj {
  id: string;
}

export interface PeoPageInfo {
  pageId: string;
  annotations: Array<PeoAnnotation>;
}

export interface PeoAnnotation {
  id: string;
  type: string;
  widgetType: string;
  rect: RectArray;
  hidden: boolean;

  name: string;
  options?: string[];
}

export interface PeoInputElement extends PeoAnnotation {
  borderWidth: number;
  fontFamily: string;
  fontSize: number;
  formattedValue: string;
  value: string;
  multiline: boolean;
  name: string;
  readonly: false;
  textAlign: string;
  textColor: string;
  type: 'Widget';
  widgetType: 'text';
}

export interface PeoChangeAction<TElement> {
  annotationId: string;
  pageId: string;
  changes: Partial<TElement>;
  type: 'annotationChanges';
}
