import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PeoChangeAction, PeoDocumentInfo, PeoPageInfo} from '../models/interfaces';
import {
  map,
} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  private serverUrl = "https://0b55-185-226-112-186.ngrok.io";

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  private getUrl(endpoint: string) {
    return `${this.serverUrl}/api${endpoint}`;
  }

  getDocumentInfo(documentId: string) {
    return this.httpClient
      .get<PeoDocumentInfo>(this.getUrl('/session/status'), {
        headers: {
          'x-peo-session': documentId,
        }
      });
  }

  getPageInfo(documentId: string, pageId: string) {
    return this.httpClient
      .get<PeoPageInfo>(this.getUrl('/edit/pageInfo'), {
        headers: {
          'x-peo-session': documentId,
        },
        params: {
          pageId,
          excludeImages: 'true',
        }
      });
  }

  getPageImage(documentId: string, pageId: string, width: number, height: number) {
    return this.httpClient
      .get(this.getUrl('/edit/pagePng'), {
        headers: {
          'x-peo-session': documentId,
        },
        params: {
          pageId,
          width: String(width),
          height: String(height),
          excludingAnnotsWithTypes: 'Widget'
        },
        responseType: 'blob',
      })
      .pipe(
        map((pageImage) => URL.createObjectURL(pageImage)),
      );
  }

  saveDoc(documentId: string) {
    return this.httpClient
      .post<void>(this.getUrl('/edit/saveDoc'), {
        headers: {
          'x-peo-session': documentId,
        }
      })
      .toPromise();
  }

  validateAndSetWidgetValue<TElement>(documentId: string, pageId: string, annotationId: string, value: string) {
    return this.httpClient
      .post<Array<PeoChangeAction<TElement>>>(this.getUrl('/edit/validateAndSetWidgetValue'), {
        pageId,
        annotationId,
        value,
      }, {
        headers: {
          'x-peo-session': documentId,
        },
      })
      .toPromise();
  }
}
