import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-doc-input',
  templateUrl: './doc-input.component.html',
  styleUrls: ['./doc-input.component.css']
})
export class DocInputComponent implements OnInit {
  @Input() controlName: string = '';
  @Input() id: string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
