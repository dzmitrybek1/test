import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-doc-dropdown',
  templateUrl: './doc-dropdown.component.html',
  styleUrls: ['./doc-dropdown.component.css']
})
export class DocDropdownComponent implements OnInit {
  @Input() controlName: string = '';
  @Input() options: string[]= [];

  constructor() { }

  ngOnInit(): void {
  }

}
