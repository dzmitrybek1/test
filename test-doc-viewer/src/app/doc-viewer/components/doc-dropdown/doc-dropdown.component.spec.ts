import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocDropdownComponent } from './doc-dropdown.component';

describe('DocDropdownComponent', () => {
  let component: DocDropdownComponent;
  let fixture: ComponentFixture<DocDropdownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocDropdownComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
